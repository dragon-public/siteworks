<?php

require_once('vendor/autoload.php');

if (!function_exists('dragon_fw_autoload_helper')) {
	function dragon_fw_autoload_helper(string $ns, string $className, string $dir) {
		if (strpos($className, $ns . '\\') !== false) {
			
			$noNamespace = substr($className, strlen($ns . '\\'));
			$normalizedClassName = str_replace('\\', '/', $noNamespace);
			$fullPathFile = __DIR__ . '/' . $dir . '/' . $normalizedClassName . '.php';
			
			if (($realPath = realpath($fullPathFile)) !== false) {
				require_once($realPath);
			}
			
		}
	}
}

spl_autoload_register(function ($className) {
	dragon_fw_autoload_helper('[[App]]', $className, 'app');
});
	