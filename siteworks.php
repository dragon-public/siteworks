<?php

use Dragon\Config;
use Dragon\Core;

/**
 * @package Siteworks
 * 
 * Plugin Name: Siteworks
 * Description: Custom plugin that handles various operations of this system. 
 * Version: 1.0.0
 * Requires PHP: 7.4+
 * Author: Red Scale Corporation
 * Text Domain: dragon-app
 * License: Proprietary
 **/

require_once('autoloader.php');

Config::$namespace = 'dragon-app';
Config::$appNamespace = '[[APP]]\\';
Config::$pluginLoaderFile = __FILE__;
Config::$pluginDir = __DIR__;

Core::init();
