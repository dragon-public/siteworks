<?php
use Dragon\Config;

return [
    'filesystems' => [
        'default' => 'local',
        
        'disks' => [
            'local' => [
                'driver' => 'local',
                'root' => Config::$pluginDir .'/storage',
            ],
        ]
    ]
];
