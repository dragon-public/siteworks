<?php

return [
	'providers' => [
		'global'		=> [
			'Role'					=> \[[App]]\Providers\Role::class,
			'Cron'					=> \[[App]]\Providers\Cron::class,
			'Ajax'					=> \[[App]]\Providers\Ajax::class,
			'Api'					=> \[[App]]\Providers\Api::class,
		],
		'admin_only'	=> [
			'UserProfile'			=> \[[App]]\Providers\UserProfile::class,
		],
		'frontend_only'	=> [
			'Shortcodes'			=> \[[App]]\Providers\Shortcodes::class,
		],
		'on_demand' => [
			'AdminInit'				=> \[[App]]\Providers\AdminInit::class,
			'AdminMenu'				=> \[[App]]\Providers\AdminMenu::class,
			'AdminPluginHooks'		=> \[[App]]\Providers\AdminPluginHooks::class,
			'FrontEndPluginHooks'	=> \[[App]]\Providers\FrontEndPluginHooks::class,
		],
	],
];
