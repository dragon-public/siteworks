<?php

namespace [[App]]\Providers;

use Dragon\Hooking\Cron as DragonCron;

class Cron extends DragonCron {
	protected static array $schedules = [
		//
	];
	
	protected static $actions = [
		//
	];
	
	protected static $hookSchedules = [
		//
	];
}
