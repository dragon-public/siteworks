<?php

namespace [[App]]\Providers;

use Dragon\Abstracts\RoleAbstract;

class Role extends RoleAbstract {
	protected static array $roles = [
		//
	];
	
	protected static array $uploadableMimes = [
		//
	];
}
