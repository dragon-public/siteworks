<?php

namespace [[App]]\Providers;

use Dragon\Abstracts\AdminMenuAbstract as DragonMenu;

class AdminMenu extends DragonMenu {
	protected static $removeMenus = [
		//
	];
	
	protected static $menus = [
		'dragon-app-settings' => [
			'menu-text'		=> 'Dragon App',
			'icon'			=> 'dashicons-welcome-view-site',
			'capabilities'	=> 'manage_options',
			'submenus' => [
				// Settings must be the top item to override root menu text, since it too is for settings.
				'dragon-app-settings' => [
					'menu-text'		=> 'Settings',
					'callback'		=> '[[App]]\\Pages\\AdminOptions',
				],
				'dragon-app-log' => [
					'menu-text'		=> 'Log',
					'page-title'	=> 'Plugin Log',
					'callback'		=> 'Dragon\\Pages\\AdminLog',
				],
			],
		],
	];
	
	protected static function getCountForMenuItem(string $menuItem) {
		return null;
	}
}

