<?php

namespace [[App]]\Providers;

use Dragon\Hooking\FrontEndPluginHooks as DragonFrontEndPluginHooks;

class FrontEndPluginHooks extends DragonFrontEndPluginHooks {
	protected static $actions = [
		//
	];
}
