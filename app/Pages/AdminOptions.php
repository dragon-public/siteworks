<?php

namespace [[App]]\Pages;

use Dragon\Pages\AdminOptionsAbstract;

class AdminOptions extends AdminOptionsAbstract {
	protected array $sections = [
		'Plugin Settings' => [
			'NAMESPACE_remove_tables_on_deactivation'	=> [
				'type'		=> 'select',
				'required'	=> true,
				'label'		=> 'Remove all plugin tables on deactivation?',
				'options'	=> [
					'no'	=> 'No (Recommended)',
					'yes'	=> 'Yes',
				],
			],
		],
	];
}
